#!/usr/bin/bash

################################################################################
# Copyright (C) 2016 by Arena of Titans Contributors.
#
# This file is part of Arena of Titans.
#
# Arena of Titans is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Arena of Titans is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Arena of Titans. If not, see <http://www.gnu.org/licenses/>.
################################################################################


set -e
set -u


function main {
    local deploy_dir="$1"
    local deploy_type="$2"
    local deploy_version="$3"
    local uwsgi_uid="${4:-}"
    local uwsgi_gid="${5:-}"
    local aot_uwsgi_file="/etc/uwsgi.d/aot-api.ini"
    local uwsgi_run_dir="/var/run/uwsgi"

    echo "********** Preparing docker image for ${deploy_type} version ${deploy_version} **********"

    pushd "${deploy_dir}" > /dev/null
        # Install dependencies
        pip3 install -U pip
        pip3 install pipenv
        pipenv install --deploy --system --dev

        echo "Run tests"
        cp -a config/config.staging.toml config/config.dev.toml
        ./setup.py test

        echo "Setuping up uwsgi"
        jinja2 --format=toml \
            -Dcurrent_dir=$(pwd) \
            -Dtype="${deploy_type}" \
            -Dversion="${deploy_version}" \
            /aot-api.dist.ini \
            "config/config.${deploy_type}.toml" > "${aot_uwsgi_file}"

        echo "Building static files"
        PYTHONPATH=$(pwd) python3 scripts/gen-boards.py -i aot/resources/games/ -o static/boards/
    popd > /dev/null

    echo "Correcting permissions"
    if [[ -n "${uwsgi_uid:-}" ]]; then
        usermod -u "${uwsgi_uid}" uwsgi
    fi
    if [[ -n "${uwsgi_gid:-}" ]]; then
        groupmod -g "${uwsgi_gid}" uwsgi
    fi

    chown -R uwsgi:uwsgi "${deploy_dir}"
    chown uwsgi:uwsgi "${aot_uwsgi_file}"
    chown root:uwsgi "${uwsgi_run_dir}"
    chmod -R u=rwX,g=rwX,o=--- "${deploy_dir}"
    chmod u=rwX,g=rwX,o=rX "${uwsgi_run_dir}"

    echo "********* Done *********"
}


main "$@"
