#!/usr/bin/bash

################################################################################
# Copyright (C) 2016 by Arena of Titans Contributors.
#
# This file is part of Arena of Titans.
#
# Arena of Titans is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Arena of Titans is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Arena of Titans. If not, see <http://www.gnu.org/licenses/>.
################################################################################


set -e
set -u


function main {
    local redis_uid="${1:-}"
    local redis_gid="${2:-}"

    if [[ -n "${redis_uid:-}" ]]; then
        usermod -u "${redis_uid}" redis
    fi
    if [[ -n "${redis_gid:-}" ]]; then
        groupmod -g "${redis_gid}" redis
    fi

    # Run dir
    mkdir /var/run/redis
    chown root:redis /var/run/redis
    chmod u=rwX,g=rwX,o=--- /var/run/redis

    # Log dir
    mkdir /var/log/redis
    chown root:redis /var/log/redis
    chmod u=rwX,g=rwX,o=--- /var/log/redis

    # Log file
    touch /var/log/redis/redis.log
    chown redis:redis /var/log/redis/redis.log
    chmod u=rw,g=rw,o=--- /var/log/redis/redis.log
}


main "$@"
