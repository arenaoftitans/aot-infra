{%- set https = type != 'testing' %}
{%- set https = type in ('prod', 'staging') -%}

{%- if type == 'prod' -%}
server {
    listen 80;
    listen 443 ssl;
    server_name arenaoftitans.com;

    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:10m;
    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS;
    ssl_certificate /etc/letsencrypt/live/arenaoftitans.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/arenaoftitans.com/privkey.pem;
    ssl_dhparam /etc/nginx/dhparam.pem;

    if ($host ~* ^arenaoftitans.com$) {
        rewrite ^(.*)$ $scheme://www.arenaoftitans.com$1 permanent;
        break;
    }
}

{% endif -%}


server {
    listen 80;
    {%- if https %}
    listen 443 ssl;
    {%- endif %}
    server_name {{ nginx.front.server_name }};

    {%- if https %}

    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:10m;
    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS;
    ssl_certificate /etc/letsencrypt/live/{{ nginx.front.server_name }}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/{{ nginx.front.server_name }}/privkey.pem;
    ssl_dhparam /etc/nginx/dhparam.pem;
    {%- endif %}

    access_log /var/log/nginx/aot-{{ type }}.access.log;
    error_log /var/log/nginx/aot-{{ type }}.error.log;

    error_page 404 /pages/404.html;
    root {{ nginx.front.root }};
    index index.html;

    disable_symlinks off;

    # Needed for cerbot to renew its certificates.
    location ~* ^/\.well-known/.+ {
        try_files $request_uri =404;
    }

    location ~ ^/game/([0-9]+)/(create.*|play/.+) {
        expires 2d;

        location ~* intl.js$ {
            try_files /$1/Intl.js =404;
        }

        try_files /$1/index.html =404;
    }

    location ~ ^/([0-9]+)/(dist|assets)/(.*)$ {
        expires 2d;

        try_files /$1/$2/$3 =404;
    }

    location / {
        expires off;
        add_header Cache-Control no-cache;

       location ~* intl.js$ {
           try_files /latest/Intl.js =404;
       }

        try_files /latest/$uri /latest/index.html =404;
    }
}
