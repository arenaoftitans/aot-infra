Arena of Titans – infra
#######################

This repository contains `ansible <https://www.ansible.com/>`__ tasks, templates and configuration to deploy the frontend and the API. It also contains scripts to test the deployment process locally and to clean old fronts and APIs from the server.

.. contents::


Requirements
============

#. `Python <https://www.python.org/>`__ 3.6 or above to run the cleanup scripts.
#. `vagrant <https://www.vagrantup.com/>`__ and `VirtualBox <https://www.virtualbox.org/wiki/Downloads>`__ to test the deployment process locally.
#. `Docker <https://www.docker.com/>`__ to run the ansible scripts locally and to build new infra images.


Quick usage guide
=================

To use these commands, you must create symlinks to the other repos:

- To the front end repo. The link must be named ``front``. For instance, use: ``ln -s ../arena-of-titans front``.
- To the API repo. The link must be named ``api``. For instance, use: ``ln -s ../aot-api api``.

Commands:

- Deploy nginx configuration: ``ansible-playbook --ask-become-pass -i environments/staging nginx.yml`` (adapt the environment if needed).
- Build and deploy only the frontend: ``ansible-playbook -i environments/staging front.yml -e version=latest`` (adapt the version and the environment if needed). You can also use ``ansible-playbook -i environments/testing deploy.yml -e version=master-2019-08-29 -e api_version=latest --tags front``.
- Build and deploy only the API: ``ansible-playbook --ask-become-pass -i environments/staging api.yml -e version=latest`` (adapt the version and the environment if needed). You can also use ``ansible-playbook --ask-become-pass -i environments/testing deploy.yml -e version=master-2019-08-29 --tags api``. Here the become password is the password of the ``aot`` user.
- Build and deploy both: ``ansible-playbook --ask-become-pass -i environments/staging deploy.yml -e version=latest`` (adapt the version and the environment if needed). Here the become password is the password of the ``aot`` user.


Project structure
=================

- ``docker``: files used to build the docker image.
- ``environments``: description of each environments (machine and variables to deploy). We have one folder per environment structured as:

  - ``hosts``: definition of the front and API machines
  - ``group_vars``: definition of variables used by group of machines (eg front or API).
  - ``host_vars``: definition of variables specific ot a machine (eg the variables used to render the templates locally).

- ``files``: some files that don't fit elsewhere.
- ``group_vars``: global variables for a group of machines.
- ``host_vars``: global variables for specific machines.
- ``rendered``: where the templates are rendered.
- ``scripts``: various scripts:

  - ``clean.py``: cleanup script to remove old front files and API containers.
  - ``docker-start-api.sh``: script that could be used to sanitize API deployment parameters and thus make sudo deployment of the API safe. This is not currently used because this script is not considered safe. We could use it with a sudoers line like: ``aotapi ALL=(aot) NOPASSWD: /usr/bin/docker -H unix\:///var/run/docker-aot.sock *, ! /usr/bin/docker *--privileged*, ! /usr/bin/docker *host*, ! /usr/bin/docker *--volume*, ! /usr/bin/docker *-v*, ! /usr/bin/docker *--publish*, ! /usr/bin/docker *-p*, ! /usr/bin/docker *--expose*``
  - ``ws.py``: to test that the newly deployed API is responsive.

- ``templates``: where the templates sit. We have templates for nginx configuration and robots.txt. The templates are rendered on the local machine and must be copied to the server manually. We don't do this automatically for security reasons (to limit root access to the server to a few persons).
- ``vagrant``: contains the Vagrantfile used to create a VirtualBox test machine to test deployment locally.
- ``ansible.cfg``: global configuration file for ansible.
- ``dk-run.sh``: wrapper script around docker to launch command in docker in the "infra" image that contains all the dependencies required for this project to run. It has the following helper command: ``deployapi`` to deploy the API, ``deployfront`` to deploy the front, ``deploystatic`` to deploy static files, ``lint`` to run the linter, ``nginx`` to render nginx templates. All other command will be passed directly to docker in order to run any command we could need. For instance: ``./dk-run.sh ansible-playbook --version``.
- ``README.rst``: this file.
- ``vagrant.sh``: helper script to perform operation with the vagrant stack. Run ``./vagrant.sh help`` to learn more.
- ``*.yml``: ansible playbooks.


Docker
======

To build the docker image (don't forget to push it!):

.. code:: bash

    docker build -f docker/Dockerfile -t registry.gitlab.com/arenaoftitans/aot-infra .


Deployment
==========

Deployments rely on gitlab CI and the steps performed to deploy a project are listed in the relevant ``.gitlab-ci.yml`` file at the root of each project.

Notes on Gitlab CI REST API
---------------------------

All the helper commands below are examples relying on the ``chore/deploy-ansible`` branch. Adapt to your needs.

- Trigger pipelines: ``curl --request POST --form token=XX --form ref=chore/deploy-ansible --form "variables[STAGE]=prod" --form "variables[API_VERSION]=the-new-version" --form "variables[VERSION]=20180216" https://gitlab.com/api/v4/projects/3859828/trigger/pipeline``
- Download artifacts: ``http --follow https://gitlab.com/arenaoftitans/arena-of-titans/-/jobs/artifacts/chore/deploy-ansible/download\?job\=build > /tmp/artifacts.zip`` or ``curl -L https://gitlab.com/arenaoftitans/arena-of-titans/-/jobs/artifacts/chore/deploy-ansible/download\?job\=build --output /tmp/artifacts.zip``
- Pipeline status: ``http https://gitlab.com/api/v4/projects/3859828/pipelines/17637216 PRIVATE-TOKEN:XXXX | jq .status``
- Pipeline trigger tokens: https://gitlab.com/arenaoftitans/arena-of-titans/settings/ci_cd under the *Pipeline triggers* section.

Overview of the process
-----------------------

Frontend
++++++++

The frontend is build on gitlab. The files are then copied with ``rsync`` (through ansible) to the server as well as a ``robots.txt`` file. Since all built files of the frontend are versioned (images and JS), we only copy the new files.

The variables used to select the version of the API will be set during the build.

The variables below must be set on gitlab for the deployment to work:

- ``SSH_PRIVATE_KEY``: SSH key used to connect to the server.

API
+++

The API is not directly accessible to anyone. We rely on the nginx HTTP server to run multiple web applications on the server. The request is then forwarded to the `gateway <https://gitlab.com/arenaoftitans/aot-gateway/>`__ which is responsible of finding the container of the asked version of the API. It then proxies the request to the API. More on why it works like this below.

The API is deployed with Ansible. View above for how to do this.

Once image of the API is deploy, we register the API as the latest API on the server with something like: ``http POST localhost:8180/register/latest <<<'{"name": "new-api"}'``. This must be run locally on the server because the gateway is not accessible from outside.

Gateway
^^^^^^^

We can test if the gateway is up by running on the server (you may need to adapt the port depending on the environment you want to test):

- ``curl localhost:8180/ws/latest`` (must result in *Bad Request* since this is a websocket endpoint).
- ``curl localhost:8180`` (must result in *Not Found* since there is nothing there).

Pickle
^^^^^^

Generate pickle signing key:

.. code:: python

    import secrets
    secrets.token_urlsafe(100)

nginx
-----

- Existing certificate are automatically renewed.
- Create a new certificate with something like: ``certbot certonly --webroot --webroot-path /data/nginx/aot/prod/api/ -d api.last-run.com``.
- To add a certificate to an existing one: ``certbot certonly --webroot --webroot-path /data/nginx/aot/prod/front/ -d last-run.com -d last-run.fr -d www.last-run.fr`` (don't forget to restart nginx).

Server
------

To setup the server, you can take a look at ``vagrant/Vagrantfile`` since it is used to setup the testing server. We document here what must be done nonetheless and give information on why it works like that.

The docker daemon used to run the containers is dedicated to AoT. This is meant to:

- Isolate AoT from the rest of the system as much as possible.
- Eventually give access to more people to the AoT docker.
- Make sure root in the container is mapped to the ``aot`` user on the server and all other users to the proper subuid and subgid.

We also want each environment to be isolated. So we put each environment in its own network with its own redis instance. We rely on an instance of the gateway to communicate outside the docker network. To do that, each instance of the gateway (one per environment) expose a port on the server. nginx forwards API request to this port. To communicate with an API, the front must ask either the latest version of the API or a specific version. This specific version must correspond to a name of an API container. The gateway can then ask forward the message to the proper container thanks to the DNS server integrated into docker.

To achieve this, we:

#. Setup the subuig and subgid for the ``aot`` user.
#. Setup docker with ``files/docker@.service`` and ``systemctl start docker@aot && systemctl enable docker@aot``.
#. Setup the ``.env`` files container the environment variables (in ``~aot/.private/aot/testing.env`` for testing):

   .. code::

    STAGE=staging
    CACHE_HOST=redis-staging
    CACHE_SIGN_KEY='XXXX'
    SENTRY_DSN=XXXX

   The use of this file is a compromise since the use of docker secrets would be more appropriate. But they require docker swarm and we don't have time to setup it up now.

#. Create the networks:

   .. code:: bash

    docker -H unix:///var/run/docker-aot.sock network create --attachable --driver bridge --subnet 172.19.0.0/16 aot-testing
    docker network create --attachable --driver bridge --subnet 172.80.0.0/16 staging
    docker network create --attachable --driver bridge --subnet 172.81.0.0/16 prod

#. Deploy redis and the gateway in the proper network:

   .. code:: bash

    # With ansible:
    ansible-playbook --ask-become-pass -i environments/testing api-infra.yml

    # Or manually with (you many need to add -H unix:///var/run/docker-aot.sock)
    STAGE=testing
    GATEWAY_SECRET=xxx
    GATEWAY_PORT=8180
    GATEWAY_ALLOWED_IPS='127.0.0.1,::1'
    docker run \
        --detach \
        --name "redis-${STAGE}" \
        --network "${STAGE}" \
        --hostname "redis.${STAGE}" \
        --restart unless-stopped \
        redis
    docker run \
        --detach \
        --name "aot-gateway-${STAGE}" \
        --network "${STAGE}" \
        --hostname "aot-gateway.${STAGE}" \
        --restart unless-stopped \
        --publish "8280:8000" \
        -e REDIS_URL="redis://redis-${STAGE}:6379" \
        -e API_HOST="" \
        -e GATEWAY_SECRET="${GATEWAY_SECRET}" \
        registry.gitlab.com/arenaoftitans/aot-gateway:23.08.1

#. We can now deploy an API with ansible or:

   .. code:: bash

    VERSION=master
    docker -H unix:///var/run/docker-aot.sock run \
        --detach \
        --name "api-${VERSION}-${STAGE}" \
        --network "${STAGE}" \
        --restart unless-stopped \
        "registry.gitlab.com/arenaoftitans/arena-of-titans-api:${VERSION}"

.. note:: Port must be 8180 for staging and 8280 for production. The GATEWAY_SECRET is read here in the ``.env`` file.

To update, do this:

#. Stop redis ``docker stop redis-${STAGE} && docker rm redis-${STAGE}``.
#. Create new redis instance ``docker run --detach --name redis-${STAGE} --network ${STAGE} --hostname redis.${STAGE} --restart unless-stopped redis:6``.
#. Stop gateway: ``docker stop aot-gateway-${STAGE} && docker rm aot-gateway-${STAGE}``
#. Deploy new version::

    docker run \
         --detach \
        --name "aot-gateway-${STAGE}" \
        --network "${STAGE}" \
        --hostname "aot-gateway.${STAGE}" \
        --restart unless-stopped \
        --publish "8280:8000" \
        -e REDIS_URL="redis://redis-${STAGE}:6379" \
        -e API_HOST="" \
        -e GATEWAY_SECRET="${GATEWAY_SECRET}" \
        registry.gitlab.com/arenaoftitans/aot-gateway:23.08.1

.. note:: Port must be 8180 for staging and 8280 for production. The GATEWAY_SECRET is read here in the ``.env`` file.

Users
+++++

We have the following users:

- ``aotfront`` to deploy the frontend. It has the proper permissions on the frontend deployment folder and is used by gitlab to deploy the frontend.
- ``aotapi`` to deploy the API. It is only accessible to a restricted number of persons.
- ``aot`` for debug and maintenance. It should belong to the ``docker`` group. It is also the user used as root in the containers. You should add ``alias docker="sudo docker -H unix:///var/run/docker-aot.sock"`` to ease communication with the AoT docker instance.


Maintenance
+++++++++++

To cleanup front and APIs, use ``./scripts/clean.py``. Run ``./scripts/clean.py --help`` to learn how to use it. By default, it will:

- Clean all frontend that are more than 5 days. It will keep the latest one. To do so, it computes:

  #. the list of all frontend files
  #. read the manifests of frontends to keep
  #. remove the files to keep from the list of all files
  #. delete what's left.

- Clean all APIs older than 5 days. It will keep the latest one.

Docker
######

To cleanup all stopped container and all images that are not used by a running container:

.. code::

    docker -H unix:///var/run/docker-aot.sock container prune --force
    docker -H unix:///var/run/docker-aot.sock image prune --all --force


Local testing
=============

To test the deployment locally (with the testing environment), you must start the vagrant machine. Use ``./vagrant.sh up`` for that. You can then edit your ``/etc/hosts`` with:

.. code::

    192.168.65.20 aot.testing
    192.168.65.20 api.aot.testing

To access the testing server more easily.

You can then use ``./dk-run.sh`` to deploy (it will target the testing environment by default).


Utility scripts
===============

Build game board
----------------

#. Install the ``geckodriver`` by downloading the executable from https://github.com/mozilla/geckodriver/releases and putting it in a location that is in your PATH.
#. Optimize the size of the SVG with `svgo <https://www.npmjs.com/package/svgo>`__. Example: ``svgo -i new_board_last_run_test_2.svg -o new_board_last_run_test_2.min.svg`` You may need to wait to do this since it may remove layers that are useful. One possible option is to run this to simply the symbols of the squares (they are the complex one we want to optimize) and then run all the step to update only the squares layer.
#. Run ``./scripts/build-game-board-from-source-svg.py --help`` to view options.
#. Last successful run: ``./scripts/build-game-board-from-source-svg.py ~/Downloads/new_board_last_run_highlight.svg --departure-squares 6,10 6,9 6,8 6,7``
#. In ``game/board-data.json`` copy everything under the ``board`` key into ``aot/game/config/boards/standard.json`` (in the API).
#. Do the same for the content of the ``test-board`` key into ``test.json``.
#. Copy the content of ``game/board.svg`` into ``app/game/play/widgets/board/board.html`` to replace the SVG part of the board.
#. Make sur the SVG is defined with ``preserveAspectRatio="none"``.


Contributing
============

Be sure that (this can be configured in your text editor or your IDE):

- Your files are encoded in UTF-8
- You use Unix style line ending (also called LF)
- You remove the trailing whitespaces
- You pull your code using ``git pull --rebase=preserve``

Code style
----------

- Wrap your code in 100 characters to ease reading.
- Use spaces, not tabs.

git hooks
---------

git hooks allow you to launch a script before or after a git command. They are very handy to automatically perform checks. If the script exits with a non 0 status, the git command will be aborted. You must write them in the `.git/hooks/` folder in a file following the convention: ``<pre|post>-<git-action>``. You must not forget to make them executable, eg: ``chmod +x .git/hooks/pre-commit``.

In the case you don't want to launch the hooks, append the ``--no-verify`` option to the git command you want to use.

pre-commit
++++++++++

.. code:: bash

   #!/usr/bin/env bash

   set -e

   flake8 --max-line-length 99 --exclude "conf.py" --exclude "aot/test" --ignore none aot
   flake8 --max-line-length 99 --ignore=F811,F401 aot/test/

pre-push
++++++++

This is only useful if you don't use ``npm run tdd`` during development.

.. code:: bash

   #!/usr/bin/env bash

   set -e

   python3 setup.py test

Commit
------

We try to follow the same `rules as the angular project <https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines>`__ towards commits. Each commit is constituted from a summary line, a body and eventually a footer. Each part are separated with a blank line.

The summary line is as follow: ``<type>(<scope>): <short description>``. It must not end with a dot and must be written in present imperative. Don't capitalize the fist letter. The whole line shouldn't be longer than 80 characters and if possible be between 70 and 75 characters. This is intended to have better logs.

The possible types are :

- chore for changes in the build process or auxiliary tools.
- doc for documentation
- feat for new features
- ref: for refactoring
- style for modifications that not change the meaning of the code.
- test: for tests

The body should be written in imperative. It can contain multiple paragraph. Feel free to use bullet points.

Use the footer to reference issue, pull requests or other commits.

This is a full example:

::

   feat(css): use CSS sprites to speed page loading

   - Generate sprites with the gulp-sprite-generator plugin.
   - Add a build-sprites task in gulpfile

   Close #24
