#!/usr/bin/env bash

set -e
set -u

readonly VAGRANT_DIR=vagrant/

help() {
    echo "Small script to manage the testing infrastructure with vagrant."
    echo "Usage:"
    echo -e "\t- ./vagrant.sh boostrap: boostrap the testing infra."
    echo -e "\t- ./vagrant.sh destroy: destroy the testing infra."
    echo -e "\t- ./vagrant.sh resume: resume the suspended testing infra."
    echo -e "\t- ./vagrant.sh ssh: ssh to the testing infra."
    echo -e "\t- ./vagrant.sh suspend: suspend the suspended testing infra."
    echo -e "\t- ./vagrant.sh vagrant VAGRANT_CMD: execute VAGRANT_CMD with vagrant on the testing infra"
}

main() {
    case "${1:-}" in
        bootstrap)
            bootstrap;;
        destroy)
            destroy;;
        resume)
            resume;;
        ssh)
            ssh;;
        suspend)
            suspend;;
        vagrant)
            # Remove vagrant from the list of args.
            shift
            vagrant_exec "$@";;
        help)
            help;;
        *)
            help
            exit 1;;
    esac
}

bootstrap() {
    pushd "${VAGRANT_DIR}" > /dev/null
        vagrant up
    popd > /dev/null
    ssh-keygen -R api.aot.testing
    ssh-keygen -R aot.testing
    ssh-add /home/jenselme/.vagrant.d/insecure_private_key
    echo "Enter root password for both playbook"
    ansible-playbook --ask-become-pass -i environments/testing nginx.yml
    ansible-playbook --ask-become-pass -i environments/testing api-infra.yml
    echo "Deploy with 'ansible-playbook --ask-become-pass -i environments/staging deploy.yml -e version=latest"
    echo "Visit http://aot.testing to see the newly deployed frontend"
}

destroy() {
    pushd "${VAGRANT_DIR}" > /dev/null
        vagrant destroy
    popd > /dev/null
}

resume() {
    pushd "${VAGRANT_DIR}" > /dev/null
        vagrant resume
    popd > /dev/null
}

ssh() {
    pushd "${VAGRANT_DIR}" > /dev/null
        vagrant ssh
    popd > /dev/null
}

suspend() {
    pushd "${VAGRANT_DIR}" > /dev/null
        vagrant suspend
    popd > /dev/null
}

vagrant_exec() {
    pushd "${VAGRANT_DIR}" > /dev/null
        vagrant "$@"
    popd > /dev/null
}

main "$@"
