#!/usr/bin/env bash

################################################################################
# Copyright (C) 2016 by Arena of Titans Contributors.
#
# This file is part of Arena of Titans.
#
# Arena of Titans is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Arena of Titans is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Arena of Titans. If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Don't allow undefined variables.
set -u
# Exit on error
set -e
# Exit on error in pipes
set -o pipefail


# Initializing associative array so they can be filled in user configuration
declare -A API_HOSTS
declare -A DEPLOY_API_BRANCHES
declare -A DEPLOY_BASE_DIRS
declare -A DEPLOY_HOSTS
declare -A DEPLOY_USERS

# Defined variables that can change per user before loading the user configuration
UWSGI_USER="uwsgi"
UWSGI_GROUP="uwsgi"
UWSGI_DEPLOY_FOLDER="/etc/uwsgi.d"
UWSGI_SOCKET_FOLDER="/var/run/uwsgi"
FRONT_LOCATION="../arena-of-titans"
## Commands
JINJA2_CLI_CMD="/usr/bin/jinja2"

# Load user configuration
source ./cli-conf.sh 2> /dev/null || echo "No user configuration file for deploy found. Testing will not be available." >&2

# Values that don't change neither per user nor per type
API_GIT_URL="https://bitbucket.org/arenaoftitans/arena-of-titans-api.git"
API_RETRIES_TIME=3
DOCKER_DIR="scripts/docker"
INTLJS_POLYFILL="node_modules/intl/dist/Intl.js"
TRACKING_SCRIPT="scripts/tracking.js"
MAX_API_RETRIES=5

# Global variables whose value is set in main.
API_HOST=''
DEPLOY_API_BRANCH=''
DEPLOY_HOST=''
DEPLOY_USER=''

# Set production and staging values for variables that changes per user and per type
API_HOSTS["prod"]="https://api.arenaoftitans.com"
API_HOSTS["staging"]="https://devapi.arenaoftitans.com"
DEPLOY_API_BRANCHES["prod"]="master"
# Always use current branch for staging deploy
DEPLOY_API_BRANCHES["staging"]=$(git rev-parse --abbrev-ref HEAD)
DEPLOY_BASE_DIRS["prod"]="/home/aot"
DEPLOY_BASE_DIRS["staging"]="/home/aot"
DEPLOY_USERS["prod"]="aot"
DEPLOY_USERS["staging"]="aot"
DEPLOY_HOSTS["prod"]="arenaoftitans.com"
DEPLOY_HOSTS["staging"]="arenaoftitans.com"


# Load tasks files
source scripts/deploy.sh
source scripts/collect.sh


usage() {
    echo "Deploy script for arena of titans. Usage:

$0 CMD TYPE

- CMD: deploy, collect, collect-on-server, templates
- TYPE: prod, staging, testing"
}


execute-on-server() {
    local commands="$1"

    # If xtrace is enabled locally, we pass it to the server
    if shopt -qo xtrace; then
        commands="set -x; $commands"
    fi

    ssh "${DEPLOY_USER}@${DEPLOY_HOST}" "${commands}"
}


exit-if-git-unclean() {
    local git_status_output=$(git status --porcelain)
    if [[ -n "${git_status_output}" ]]; then
        echo "Uncommited changes in $(pwd). Exiting" >&2
        exit 1
    fi
}


main() {
    if [[ "$#" != 2 ]]; then
        echo "Invalid number of arguments." >&2
        usage
        exit 1
    fi

    local cmd="$1"
    local type="$2"
    local version="$(date '+%Y%m%d%H%M%S')"

    case "${cmd}" in
        'templates')
            mkdir -p generated
            ${JINJA2_CLI_CMD} --format=toml \
                -Dtype="${type}" \
                templates/aot-api.dist.conf \
                "config/config.${type}.toml" > generated/aot-api.conf
            ${JINJA2_CLI_CMD} --format=toml \
                -Dtype="${type}" \
                templates/aot.dist.conf \
                "config/config.${type}.toml" > generated/aot.conf
            exit 0;;
    esac

    # For deployment related commands, we start by the type.
    case "${type}" in
        'prod'|'staging'|'testing')
            if [[ "${type}" == "testing" ]] && [[ -z "${API_HOSTS['testing']:-}" ]]; then
                echo "Testing is not available. Exiting" >&2
                exit 1
            fi

            API_HOST="${API_HOSTS[${type}]}"
            DEPLOY_API_BRANCH="${DEPLOY_API_BRANCHES[${type}]}"
            DEPLOY_BASE_DIR="${DEPLOY_BASE_DIRS[${type}]}"
            DEPLOY_HOST="${DEPLOY_HOSTS[${type}]}"
            DEPLOY_USER="${DEPLOY_USERS[${type}]}"

            case "${cmd}" in
                'deploy')
                    deploy "${type}" "${version}";;
                'collect')
                    collect "${type}";;
                'collect-on-server')
                    collect-on-server "${type}";;
                *)
                    usage
                    exit 1;;
            esac;;
        'dev')
            echo 'Cannot deploy for dev' >&2
            exit 1;;
        *)
            usage
            exit 1;;
    esac
}

main "$@"

