################################################################################
# Copyright (C) 2016 by Arena of Titans Contributors.
#
# This file is part of Arena of Titans.
#
# Arena of Titans is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Arena of Titans is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Arena of Titans. If not, see <http://www.gnu.org/licenses/>.
################################################################################


collect() {
    local type="$1"

    execute-on-server "set -u && \
        set -e && \
        export DEPLOY_BASE_DIR=\"${DEPLOY_BASE_DIR}\" && \
        export DOCKER_DIR=\"${DOCKER_DIR}\" && \
        $(declare -f collect-on-server) && \
        $(declare -f docker-stop-api) && \
        $(declare -f _collect-api-on-server) && \
        $(declare -f _collect-front-on-server) && \
        collect-on-server ${type}"
}


collect-on-server() {
    local type="$1"
    local api_dir="${DEPLOY_BASE_DIR}/${type}/api"
    local front_dir="${DEPLOY_BASE_DIR}/${type}/front"
    local versions_to_collect=()
    local latest
    local redis_socket

    pushd "${api_dir}" > /dev/null
        latest=$(readlink -f latest)
        latest="${latest##*/}"
        for version in $(ls -1 | grep -v latest); do
            # If redis failed to deploy, the socket won't exist but we need to collect the version anyway
            redis_socket="${version}/sockets/redis.sock"
            if [[ ! -S "${redis_socket}" ]]; then
                versions_to_collect+=("${version}")
            else
                # The deploy script creates a test key in the database. We ignore it when
                # collecting.
                # Since if no match is found, grep will exit with a non zero status code,
                # we use `|| true` to allow the script to continue.
                keys=$(redis-cli -s "${redis_socket}" keys \* | { grep -v test || true; })
                if [[ -z "${keys}" && "${version}" != "${latest}" ]]; then
                    versions_to_collect+=("${version}")
                fi
            fi
        done
    popd > /dev/null

    # No versions to collect
    if [[ "${#versions_to_collect[@]}" == 0 ]]; then
        echo "No version to collect."
        exit 0
    fi

    _collect-front-on-server
    _collect-api-on-server

    echo "Collect is done"
}


_collect-front-on-server() {
    # versions_to_collect and front_dir must come from caller

    pushd "${front_dir}" > /dev/null
        for version in "${versions_to_collect[@]}"; do
            echo "Collecting front for ${version}"
            rm -rf "${version}"
        done
    popd > /dev/null
}


_collect-api-on-server() {
    # versions_to_collect, type and api_dir must come from caller

    pushd "${api_dir}" > /dev/null
        for version in "${versions_to_collect[@]}"; do
            echo "Collecting api for ${version}"
            docker-stop-api "${version}"

            echo -e "\tDelete API files"
            rm -rf "${version}"
        done
    popd > /dev/null
}
