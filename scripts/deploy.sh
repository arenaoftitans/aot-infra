################################################################################
# Copyright (C) 2016 by Arena of Titans Contributors.
#
# This file is part of Arena of Titans.
#
# Arena of Titans is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Arena of Titans is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Arena of Titans. If not, see <http://www.gnu.org/licenses/>.
################################################################################


deploy() {
    local type="$1"
    local version="$2"

    # Save info about deployed infra
    echo "Infra
=====
" > "${INFO_FILE}"
    git log -1 --format="commit %h%nAuthor: %an <%ae>%nDate: %ad%nSubject %s%n%n" >> "${INFO_FILE}"

    echo "Updating templates"
    generate "${type}"
    echo "Deploying frontend for ${type} with version ${version}"
    deploy-front "${type}" "${version}"
    echo "Deploying API"
    deploy-api "${type}" "${version}"

    # Make deployed version lastest version
    echo "Updating default version for ${type} with version ${version}"
    execute-on-server "ln --force --no-dereference --symbolic ${DEPLOY_BASE_DIR}/${type}/front/${version} ${DEPLOY_BASE_DIR}/${type}/front/latest"
    execute-on-server "ln --force --no-dereference --symbolic ${DEPLOY_BASE_DIR}/${type}/api/${version} ${DEPLOY_BASE_DIR}/${type}/api/latest"
    scp -P 222 "${INFO_FILE}" "${DEPLOY_USER}@${DEPLOY_HOST}:${DEPLOY_BASE_DIR}/${type}/api/latest"
    echo "Deploy is done"
}


deploy-front() {
    local type="$1"
    local version="$2"
    local front_dir="${DEPLOY_BASE_DIR}/${type}/front/${version}"
    local environment
    local api_token
    local token

    execute-on-server "mkdir -p ${front_dir}"
    scp -P 222 "generated/robots.front.${type}.txt" "${DEPLOY_USER}@${DEPLOY_HOST}:${front_dir}/robots.txt"
    pushd "${FRONT_LOCATION}" > /dev/null
        # Save info about deployed front
        echo "Front
=====
" >> "${INFO_FILE}"
        git log -1 --format="commit %h%nAuthor: %an <%ae>%nDate: %ad%nSubject %s%n%n" >> "${INFO_FILE}"

        release "${type}" "${version}"
        echo -e "\tBuilding frontend"

        # Preparing rollbar
        if [[ -f "${ROLLBAR_LOCAL_CONFIG}" ]]; then
            token=$(cat "${ROLLBAR_LOCAL_CONFIG}" | jq -r ".${type}.front.access_token")
            environment=$(cat "${ROLLBAR_LOCAL_CONFIG}" | jq -r ".${type}.front.environment")
            sed -Ei "s/\"POST_CLIENT_ITEM_ACCESS_TOKEN\"/\"${token}\"/" scripts/rollbar.js
            sed -Ei "s/\"ENVIRONMENT\"/\"${environment}\"/" scripts/rollbar.js
        fi

        npm run clean > /dev/null
        npm run config -- --type "${type}" --version "${version}" > /dev/null
        npm run buildprod -- --version "${version}" > /dev/null

        echo -e "\tPushing code to server"
        # Correct load path so scripts are loaded from the correct version.
        sed -Ei "s#\"(/dist/[a-z\-]+bundle.js)\"#\"/${version}\1\"#g" index.html
        sed -Ei "s#\"(/tracking.js)\"#\"/${version}\1\"#g" index.html
        sed -Ei "s#\"(/dist/[a-z\-]+bundle)\"#\"/${version}\1\"#g" dist/vendor-bundle.js
        # Correct assets path in index
        sed -Ei "s#\"(/assets/.*)\"#\"/${version}\1\"#g" index.html

        # Sync with server
        rsync -e 'ssh -p 222' -a --delete --info=progress2 --exclude="*.map" "index.html" "assets" "dist" "${DEPLOY_USER}@${DEPLOY_HOST}:${front_dir}"
        scp -P 222 "${INTLJS_POLYFILL}" "${TRACKING_SCRIPT}" "${DEPLOY_USER}@${DEPLOY_HOST}:${front_dir}"

        # Register deploy and source maps
        if [[ -f "${ROLLBAR_LOCAL_CONFIG}" ]]; then
            api_token=$(cat "${ROLLBAR_LOCAL_CONFIG}" | jq -r ".${type}.api.access_token")
            curl https://api.rollbar.com/api/1/deploy/ \
                -F access_token="${api_token}" \
                -F environment="${environment}" \
                -F revision="$(git rev-parse HEAD)" \
                -F local_username="$(id --user --name)"
            curl https://api.rollbar.com/api/1/sourcemap \
                -F access_token="${api_token}" \
                -F version="$(git rev-parse HEAD)" \
                -F minified_url="https://dev.arenaoftitans.com/${version}/dist/common-bundle.js" \
                -F source_map=@dist/common-bundle.js.map
            curl https://api.rollbar.com/api/1/sourcemap \
                -F access_token="${api_token}" \
                -F version="$(git rev-parse HEAD)" \
                -F minified_url="https://dev.arenaoftitans.com/${version}/dist/game-common-bundle.js" \
                -F source_map=@dist/game-common-bundle.js.map
            curl https://api.rollbar.com/api/1/sourcemap \
                -F access_token="${api_token}" \
                -F version="$(git rev-parse HEAD)" \
                -F minified_url="https://dev.arenaoftitans.com/${version}/dist/game-create-bundle.js" \
                -F source_map=@dist/game-create-bundle.js.map
            curl https://api.rollbar.com/api/1/sourcemap \
                -F access_token="${api_token}" \
                -F version="$(git rev-parse HEAD)" \
                -F minified_url="https://dev.arenaoftitans.com/${version}/dist/game-play-bundle.js" \
                -F source_map=@dist/game-play-bundle.js.map
            curl https://api.rollbar.com/api/1/sourcemap \
                -F access_token="${api_token}" \
                -F version="$(git rev-parse HEAD)" \
                -F minified_url="https://dev.arenaoftitans.com/${version}/dist/site-bundle.js" \
                -F source_map=@dist/site-bundle.js.map
        fi

        # Reset index.html
        git checkout -q index.html
        # Reset Rollbar script
        git checkout -q scripts/rollbar.js
    popd > /dev/null
}


release() {
    local type="$1"
    local version="$2"

    echo -e "\tReleasing"

    git push -q
}


deploy-api() {
    local type="$1"
    local version="$2"
    local cfg_file
    local deployed_commit

    if [[ "${type}" == 'prod' ]]; then
        exit-if-git-unclean
    fi
    release "${type}" "${version}"
    if [[ "${type}" == "testing" ]]; then
        cfg_file="/tmp/config.testing.toml-${version}"
        scp -P 222 "${API_LOCATION}/config/config.testing.toml" "${DEPLOY_USER}@${DEPLOY_HOST}:/tmp/config.testing.toml-${version}"
    fi

    pushd "${API_LOCATION}" > /dev/null
        # Save info about deployed front
        echo "API
===
" >> "${INFO_FILE}"
        deployed_commit=$(git rev-parse "${DEPLOY_API_BRANCH}")
        git show "${deployed_commit}" -s --format="commit %h%nAuthor: %an <%ae>%nDate: %ad%nSubject %s%n%n" >> "${INFO_FILE}"
    popd > /dev/null

    execute-on-server "set -u && \
        set -e && \
        $(declare -f deploy-api-server) && \
        $(declare -f wait-api-to-start) && \
        $(declare -f ws) && \
        $(declare -f test-api) && \
        $(declare -f docker-refresh) && \
        $(declare -f docker-start-api) && \
        $(declare -f docker-build-bases) && \
        $(declare -f docker-clean-images) && \
        export API_GIT_URL=\"${API_GIT_URL}\" && \
        export API_HOST=\"${API_HOST}\" && \
        export API_RETRIES_TIME=\"${API_RETRIES_TIME}\" && \
        export DOCKER_DIR=\"${DOCKER_DIR}\" && \
        export DEPLOY_API_BRANCH=\"${DEPLOY_API_BRANCH}\" && \
        export DEPLOY_BASE_DIR=\"${DEPLOY_BASE_DIR}\" && \
        export INFRA_GIT_URL=\"${INFRA_GIT_URL}\" && \
        export MAX_API_RETRIES=\"${MAX_API_RETRIES}\" && \
        export ROLLBAR_CONFIG=\"${ROLLBAR_CONFIG}\" && \
        deploy-api-server ${type} ${version} ${cfg_file:-}"
    # Copy robots.txt
    scp -P 222 "generated/robots.api.${type}.txt" "${DEPLOY_USER}@${DEPLOY_HOST}:${DEPLOY_BASE_DIR}/${type}/api/${version}/robots.txt"
}


deploy-api-server() {
    local type="$1"
    local version="$2"
    local cfg_file="${3:-}"
    local api_dir="${DEPLOY_BASE_DIR}/${type}/api/${version}"

    mkdir -p "${api_dir}"
    pushd "${api_dir}" > /dev/null
        git clone -q "${INFRA_GIT_URL}" .

        docker-refresh

        pushd "${DOCKER_DIR}/aot-api" > /dev/null
            echo -e "\tCloning API from ${API_GIT_URL} to track branch ${DEPLOY_API_BRANCH}"
            git clone -q --branch "${DEPLOY_API_BRANCH}" "${API_GIT_URL}" aot-api
            pushd aot-api > /dev/null
                if [[ -n "${cfg_file:-}" ]] && [[ -f "${cfg_file:-}" ]]; then
                    mv "${cfg_file}" config/config.testing.toml
                fi
            popd > /dev/null
        popd > /dev/null

        docker-start-api "${type}" "${version}"
        # Create the symlink for /ws/latest to work
        pushd sockets > /dev/null
            ln -s "aot-api-ws-${type}-${version}.sock" "aot-api-ws-${type}-latest.sock"
        popd > /dev/null

        echo -e "\tWaiting for API to start"
        wait-api-to-start "${type}" "${version}"
        test-api "${type}" "${version}"
    popd > /dev/null

}


wait-api-to-start() {
    local type="$1"
    local version="$2"
    local retries=0

    while ! curl -f "${API_HOST}/ws/${version}" > /dev/null 2>&1 && [[ "${retries}" < "${MAX_API_RETRIES}" ]]; do
        sleep "${API_RETRIES_TIME}"
        # Maths operation returns their value, which means ((retries++)) will return with non zero
        # status code and so exit the script.
        ((retries++)) || true
    done

    if [[ "${retries}" == "${MAX_API_RETRIES}" ]]; then
        echo "Api could not start. Exiting" >&2
        exit 1
    fi
}


test-api() {
    local type="$1"
    local version="$2"
    local http_url="${API_HOST}/ws/${version}"
    local ws_url="${http_url/http/ws}"

    ws test "${ws_url}"
}
