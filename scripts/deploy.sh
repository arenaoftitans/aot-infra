################################################################################
# Copyright (C) 2016 by Arena of Titans Contributors.
#
# This file is part of Arena of Titans.
#
# Arena of Titans is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Arena of Titans is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Arena of Titans. If not, see <http://www.gnu.org/licenses/>.
################################################################################


deploy() {
    local type="$1"
    local version="$2"

    echo "Deploying frontend for ${type} with version ${version}"
    deploy-front "${type}" "${version}"
    echo "Deploying API"
    deploy-api "${type}" "${version}"

    # Make deployed version lastest version
    echo "Updating default version for ${type} with version ${version}"
    execute-on-server "ln --force --no-dereference --symbolic ${DEPLOY_BASE_DIR}/${type}/front/${version} ${DEPLOY_BASE_DIR}/${type}/front/latest"
    execute-on-server "ln --force --no-dereference --symbolic ${DEPLOY_BASE_DIR}/${type}/api/${version} ${DEPLOY_BASE_DIR}/${type}/api/latest"
    execute-on-server "sudo ln -sf ${UWSGI_SOCKET_FOLDER}/aot-api-ws-${type}-${version}.sock ${UWSGI_SOCKET_FOLDER}/aot-api-ws-${type}-latest.sock"
    echo "Deploy is done"
}


deploy-front() {
    local type="$1"
    local version="$2"
    local front_dir="${DEPLOY_BASE_DIR}/${type}/front/${version}"

    execute-on-server "mkdir -p ${front_dir}"
    pushd "${FRONT_LOCATION}" > /dev/null
        release "${type}" "${version}"
        echo -e "\tBuilding frontend"
        npm run clean > /dev/null
        npm run config -- --type "${type}" --version "${version}" > /dev/null
        npm run buildprod > /dev/null

        echo -e "\tPushing code to server"
        # Correct load path so scripts are loaded from the correct version.
        sed -Ei "s#\"(/dist/[a-z\-]+bundle.js)\"#\"/${version}\1\"#g" index.html
        sed -Ei "s#\"(/dist/[a-z\-]+bundle)\"#\"/${version}\1\"#g" dist/vendor-bundle.js
        # Correct assets path in index
        sed -Ei "s#\"(/assets/.*)\"#\"/${version}\1\"#g" index.html
        # Correct assets path in included HTML
        sed -Ei "s#\"(/assets/[^\"]*)\"#\"/${version}\1\"#g" dist/*.js
        # Correct assets path in included CSS
        sed -Ei "s#url\((/assets/[^\)]*)\)#url(/${version}\1)#g" dist/*.js

        # Sync with server
        rsync -a --delete --info=progress2 --exclude="*.map" "index.html" "assets" "dist" "${DEPLOY_USER}@${DEPLOY_HOST}:${front_dir}"
        scp "${INTLJS_POLYFILL}" "${TRACKING_SCRIPT}" "${DEPLOY_USER}@${DEPLOY_HOST}:${front_dir}"

        # Reset index.html
        git checkout -q index.html
    popd > /dev/null
}


release() {
    local type="$1"
    local version="$2"

    echo -e "\tReleasing"

    git push -q

    if [[ "${type}" == "prod" ]]; then
        git tag "${version}"
        git push -q --tags
    fi
}


deploy-api() {
    local type="$1"
    local version="$2"
    local cfg_file

    if [[ "${type}" == 'prod' ]]; then
        exit-if-git-unclean
    fi
    release "${type}" "${version}"
    if [[ "${type}" == "testing" ]]; then
        cfg_file="/tmp/config.testing.toml-${version}"
        scp "config/config.testing.toml" "${DEPLOY_USER}@${DEPLOY_HOST}:/tmp/config.testing.toml-${version}"
    fi

    execute-on-server "set -u && \
        set -e && \
        $(declare -f deploy-api-server) && \
        $(declare -f wait-api-to-start) && \
        export API_GIT_URL=\"${API_GIT_URL}\" && \
        export API_HOST=\"${API_HOST}\" && \
        export API_RETRIES_TIME=\"${API_RETRIES_TIME}\" && \
        export DOCKER_DIR=\"${DOCKER_DIR}\" && \
        export DEPLOY_API_BRANCH=\"${DEPLOY_API_BRANCH}\" && \
        export DEPLOY_BASE_DIR=\"${DEPLOY_BASE_DIR}\" && \
        export MAX_API_RETRIES=\"${MAX_API_RETRIES}\" && \
        deploy-api-server ${type} ${version} ${cfg_file:-}"
}


deploy-api-server() {
    local type="$1"
    local version="$2"
    local cfg_file="$3"

    local root="$(pwd)"

    docker pull fedora:24
    docker pull redis:latest

    pushd "${DOCKER_DIR}" > /dev/null
        pushd base > /dev/null
            docker build -t fedora_uwsgi .
        popd > /dev/null

        pushd aot > /dev/null
            echo -e "\tCloning API from ${API_GIT_URL} to track branch ${DEPLOY_API_BRANCH}"
            git clone -q "${API_GIT_URL}" aot-api
            pushd aot-api > /dev/null
                git checkout -q "${DEPLOY_API_BRANCH}" > /dev/null
                cp "config/config.staging.toml" "config/config.dev.toml"
                if [[ -n "${cfg_file:-}" ]] && [[ -f "${cfg_file:-}" ]]; then
                    mv "${cfg_file}" config/config.testing.toml
                fi
                make config type="${type}" version="${version}" > /dev/null
                make static type="${type}" version="${version}" > /dev/null
            popd > /dev/null
            docker build -t aot .
        popd > /dev/null
    popd > /dev/null

    docker ps -a | grep 'Exited' | awk '{print $1}' | xargs --no-run-if-empty docker rm
    docker images | grep '^<none>' | awk '{print $3}' | xargs --no-run-if-empty docker rmi

    echo -e "\tWaiting for API to start"
    wait-api-to-start "${type}" "${version}"
}


wait-api-to-start() {
    local type="$1"
    local version="$2"
    local retries=0

    while ! curl -f "${API_HOST}/ws/${version}" > /dev/null 2>&1 && [[ "${retries}" < "${MAX_API_RETRIES}" ]]; do
        sleep "${API_RETRIES_TIME}"
        # Maths operation returns their value, which means ((retries++)) will return with non zero
        # status code and so exit the script.
        ((retries++)) || true
    done

    if [[ "${retries}" == "${MAX_API_RETRIES}" ]]; then
        echo "Api could not start. Exiting" >&2
        exit 1
    fi
}

