docker-refresh() {
    local no_cache="${1:-}"

    echo "Refresh base images"
    docker pull fedora:26
    docker pull redis:latest

    docker-build-bases "${no_cache:-}"
}


docker-build-bases() {
    local no_cache="${1:-}"
    local cmd=(docker build -t aot-api-base)

    if [[ "${no_cache:-}" == "no-cache" ]]; then
        cmd+=(--no-cache)
    fi

    cmd+=("${DOCKER_DIR}/aot-api-base")

    "${cmd[@]}"

    docker-clean-images
}


docker-build() {
    docker build -t aot-api \
        --build-arg deploy_type=dev \
        --build-arg deploy_version=latest \
        --build-arg uwsgi_uid=$(id -u) \
        --build-arg uwsgi_gid=$(id -g) \
        "${DOCKER_DIR}/aot-api"
    docker build -t aot-redis \
        --build-arg redis_uid=$(id -u) \
        --build-arg redis_gid=$(id -g) \
        "${DOCKER_DIR}/aot-redis"
}


docker-run() {
    local image_name="$1"
    local host_volume="${2:-}"
    local volume
    local cmd=(docker run)

    if [[ -n "${host_volume}" ]]; then
        case "${image_name}" in
            'aot-api')
                volume="${host_volume}:/aot-api/external/:rw";;
            'aot-redis')
                volume="${host_volume}:/var/run/redis/:rw";;
            *)
                echo "Unknown image ${image_name} for usage with volume" >&2
                usage >&2
                exit 1;;
        esac
        cmd+=(--volume "${volume}")
    fi

    cmd+=("${image_name}")

    "${cmd[@]}"
}


docker-start-api() {
    local type="$1"
    local version="$2"
    local socket_dir=$(pwd)/sockets
    local logs_dir=$(pwd)/logs

    mkdir -p "${socket_dir}"
    mkdir -p "${logs_dir}"

    pushd "${DOCKER_DIR}/aot-api" > /dev/null
        # Create a subshell not to pollute the global namespace
        (
            export DEPLOY_TYPE="${type}"
            export DEPLOY_VERSION="${version}"
            export LOGS_DIR="${logs_dir}"
            export REDIS_UID=$(id -u)
            export REDIS_GID=$(id -g)
            export ROLLBAR_CONFIG="${ROLLBAR_CONFIG}"
            export SOCKET_DIR="${socket_dir}"
            export UWSGI_UID=$(id -u)
            export UWSGI_GID=$(id -g)

            mkdir -p "${SOCKET_DIR}"
            docker-compose -p aot_${DEPLOY_VERSION} up -d
        )
    popd > /dev/null
}


docker-stop-api() {
    local version="${1:-latest}"
    local docker_compose_dir="${DOCKER_DIR}/aot-api"

    if [[ ! -d "${docker_compose_dir}" ]]; then
        docker_compose_dir="${version}/${docker_compose_dir}"
    fi

    pushd "${docker_compose_dir}" > /dev/null
        docker-compose -p aot_${version} down --rmi all
    popd > /dev/null
}


docker-clean() {
    docker-clean-container
    docker-clean-images
}


docker-clean-container() {
    docker ps -a | grep -E '(Exited|Dead)' | awk '{print $1}' | xargs --no-run-if-empty docker rm
}


docker-clean-images() {
    docker images | grep '^<none>' | awk '{print $3}' | xargs --no-run-if-empty docker rmi
}
