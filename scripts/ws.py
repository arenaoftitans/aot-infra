#!/usr/bin/env python3

import asyncio
import json
import sys
import websockets

from argparse import ArgumentParser
from async_timeout import timeout


async def main(args):
    try:
        with timeout(10):
            if args.main == 'test':
                await test(args.url)
            elif args.main == 'info':
                await info(args.urls)
    except asyncio.TimeoutError:
        print('The connection took too long.', file=sys.stdout)
        sys.exit(1)


async def test(url):
    async with websockets.connect(url, timeout=5) as websocket:
        payload = json.dumps({
            'rt': 'test',
        })
        await websocket.send(payload)
        response = await websocket.recv()
        response = json.loads(response)
        if not response.get('success', False):
            print('The API did not respond correctly', file=sys.stdout)
            print(response.get('errors', None), file=sys.stdout)
            sys.exit(1)


async def info(urls):
    totals = {}

    for url in urls:
        async with websockets.connect(url, timeout=5) as websocket:
            payload = json.dumps({
                'rt': 'info',
            })
            await websocket.send(payload)
            response = await websocket.recv()
            response = json.loads(response)
            totals['average_number_players'] = totals.get('average_number_players', 0) + \
                response.get('average_number_players', 0)
            totals['number_games'] = totals.get('number_games', 0) + \
                response.get('number_games', 0)
            totals['number_started_games'] = totals.get('number_started_games', 0) + \
                response.get('number_started_games', 0)
            totals['number_connected_players'] = totals.get('number_connected_players', 0) + \
                response.get('number_connected_players', 0)
            print_report(url, response)

    number_instances = len(urls) or 1
    totals['average_number_players'] = totals.get('average_number_players', 0) / number_instances
    print_report('all', totals)


def print_report(title, report):
    print(title)
    for key, value in report.items():
        print('\t{key}: {value}'.format(key=key, value=value))
    print()


if __name__ == '__main__':
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest='main')

    test_subparser = subparsers.add_parser(
        'test',
        help='Send a test request to the API to tests whether it should work correctly',
    )
    test_subparser.add_argument(
        'url',
        help='The URL to connect to. It must include the schema and the port, '
             'eg: ws://localhost:4242',
    )

    info_subparser = subparsers.add_parser(
        'info',
        help='Send an info request to the API to get stats.',
    )
    info_subparser.add_argument(
        'urls',
        nargs='+',
        help='The URL to connect to. It must include the schema and the port, '
             'eg: ws://localhost:4242',
    )

    args = parser.parse_args()

    asyncio.get_event_loop().run_until_complete(main(args))
