#!/usr/bin/env python3

import contextlib
import logging
import json
import os

from itertools import chain
from argparse import ArgumentParser
from functools import lru_cache
from pathlib import Path

import arrow
import docker


# Expressed in days.
DEFAULT_TTL = 5


def main(args):
    clean_front(args.front_root, dry_run=args.dry_run, ttl=args.ttl)
    clean_api(
        args.docker_socket,
        args.docker_prefix,
        args.docker_suffix,
        dry_run=args.dry_run,
        ttl=args.ttl,
    )


def clean_front(root, dry_run=True, ttl=DEFAULT_TTL):
    with cd(root):
        collect_date = arrow.now().shift(days=-ttl)
        files = list_all_files()
        files_to_keep = set()
        manifests = list_manifests()
        latest_manifest = manifests[0]

        for manifest in manifests:
            manifest_age = arrow.get(manifest.stat().st_mtime)

            if manifest_age > collect_date or manifest == latest_manifest:
                files_to_keep.update(get_manifest_files(manifest))

        files_to_delete = files - files_to_keep
        delete_files(files_to_delete, root=root, dry_run=dry_run)

        logging.info(f'Removed {len(files_to_delete)} files')
        logging.info(f'Kept {len(files_to_keep)} files')


def list_all_files():
    files = Path('.').glob('**/*')
    return {file.resolve().as_posix() for file in files if file.is_file()}


def list_manifests():
    manifests = Path('.').glob('**/manifest*')

    return sorted(
        manifests,
        key=lambda manifest: manifest.stat().st_mtime,
        reverse=True,
    )


def get_manifest_files(manifest):
    fileset = set()
    manifest_files, version = read_manifest(manifest)
    fileset.update((os.path.abspath(file) for file in manifest_files))
    fileset.add(manifest.resolve().as_posix())
    fileset.add(os.path.abspath(f'index-{version}.html'))

    return fileset


@lru_cache()
def read_manifest(manifest):
    with manifest.open() as manifest_file:
        content = json.load(manifest_file)
        files = content['files']
        version = content['version']

    return files, version


def delete_files(files_to_delete, root=None, dry_run=True):
    for file in files_to_delete:
        if file.startswith(root):
            unlink(file, dry_run=dry_run)
        else:
            logging.error(f'{file} is not under {root}')


def unlink(file, dry_run=True):
    if dry_run:
        logging.info(f'deleting: {file}')
    else:
        os.unlink(file)


def clean_api(docker_socket, docker_prefix, docker_suffix, dry_run=True, ttl=DEFAULT_TTL):
    client = docker.DockerClient(base_url=docker_socket)
    containers = list_all_api_containers(client, prefix=docker_prefix, suffix=docker_suffix)
    latest_container = containers[0]
    collect_date = arrow.now().shift(days=-ttl)

    for container in containers:
        creation_date = arrow.get(container.attrs['Created'])
        if creation_date <= collect_date and container is not latest_container:
            delete_container(container, dry_run=dry_run)

    logging.info('Pruning containers and images')
    if not dry_run:
        client.containers.prune()
        client.images.prune(filters={'dangling': False})


def list_all_api_containers(client, prefix='api', suffix=''):
    containers = []

    for container in client.containers.list(all=True):
        if container.name.startswith(prefix) and container.name.endswith(suffix):
            containers.append(container)

    containers.sort(key=lambda c: c.attrs['Created'], reverse=True)

    return containers


def delete_container(container, dry_run=True):
    logging.info(f'deleting container: {container.id} ({container.name})')
    if dry_run:
        return

    container.stop()
    container.remove()


@contextlib.contextmanager
def cd(path):
    '''A context manager which changes the working directory to the given
    path, and then changes it back to its previous value on exit.

    From https://code.activestate.com/recipes/576620-changedirectory-context-manager/#c3
    '''
    prev_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    parser = ArgumentParser()
    parser.add_argument(
        '--docker-socket',
        required=True,
        help='The docker socket to which we must connect',
    )
    parser.add_argument(
        '--docker-prefix',
        required=True,
        help='Profix to filter the proper container',
    )
    parser.add_argument(
        '--docker-suffix',
        required=True,
        help='Suffix to filter the proper container',
    )
    parser.add_argument(
        '--front-root',
        required=True,
        help='The root of the frontend where the index files are.',
    )
    parser.add_argument(
        '--dry-run',
        action='store_true',
        help='Do not delete anything, print what would be',
    )
    parser.add_argument(
        '--ttl',
        default=DEFAULT_TTL,
        help='Maximum age of fronts and APIs',
    )

    args = parser.parse_args()

    main(args)
