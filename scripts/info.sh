info() {
    local type="$1"
    local instance="${2:-latest}"
    local versions=()
    local version
    local instances=''
    local instance

    if [[ "${instance}" == 'all' ]]; then
        versions=($(execute-on-server "set -u && \
            set -e && \
            ls ${DEPLOY_BASE_DIR}/${type}/api/ | grep -v latest"))
        for version in "${versions[@]}"; do
            instance="${API_HOST}/ws/${version}"
            instance="${instance/http/ws}"
            instances=("${instances} ${instance}")
        done
    else
        instance="${API_HOST}/ws/latest"
        instance="${instance/http/ws}"
        instances=("${instance}")
    fi

    _get-info
}


_get-info() {
    execute-on-server "set -u && \
        set -e && \
        cd ${DEPLOY_BASE_DIR}/${type}/api/latest && \
        ./cli.sh ws info ${instances}"
}
