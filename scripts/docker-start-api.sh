#!/usr/bin/env bash

set -e
set -u

usage() {
    echo "$0 -H DOCKER_HOST -V VERSION -e STAGE -E STAGE_FILE -r REGISTRY"
    echo "Example: $0 -H 'unix:///var/run/docker-aot.sock' -V master-18-04-15-14-25 -e testing -E /home/aot/.private/aot/testing.env -r 'registry.gitlab.com/arenaoftitans/arena-of-titans-api'"
}

main() {
    while getopts "hH:V:e:E:r:" opt; do
        case "${opt}" in
            h)
                usage
                exit 0;;
            H)
                DOCKER_HOST="${OPTARG}";;
            V)
                VERSION="${OPTARG}";;
            e)
                STAGE="${OPTARG}";;
            E)
                STAGE_FILE="${OPTARG}";;
            r)
                REGISTRY="${OPTARG}";;
            ?)
                usage
                exit 1;;
        esac
    done

    if [[ -z "${DOCKER_HOST:-}" ]] || [[ -z "${VERSION:-}" ]] || [[ -z "${STAGE:-}" ]] || [[ -z "${STAGE_FILE:-}" ]] || [[ -z "${REGISTRY:-}" ]]; then
        usage
        exit 1
    fi

    validate_docker_host "${DOCKER_HOST}"
    validate_version "${VERSION}"
    validate_env "${STAGE}"
    validate_envfile "${STAGE_FILE}"
    validate_registry "${REGISTRY}"
}

validate_docker_host() {
    local value="$1"
    if [[ "${value}" =~ ^unix:///[a-zA-Z/\\-]+\.sock$ ]]; then
        return 0
    else
        echo "docker host is invalid" >&2
        exit 1
    fi
}

validate_env() {
    local env="$1"
    if [[ "${env}" == "testing" ]] || [[ "${env}" == "staging" ]] || [[ "${env}" == "production" ]] || [[ "${env}" == "prod" ]]; then
        return 0
    else
        echo "${env} is not a valid env." >&2
        exit 1
    fi
}

validate_envfile() {
    local value="$1"
    if [[ "${value}" =~ ^/home/aot/\.private/aot/(testing|staging|prod|production)\.env$ ]]; then
        return 0
    else
        echo "envfile is invalid" >&2
        exit 1
    fi
}

validate_registry() {
    local value="$1"
    if [[ "${value}" =~ ^[a-zA-Z/.\\-]+$ ]]; then
        return 0
    else
        echo "Registry is invalid" >&2
        exit 1
    fi
}

validate_version() {
    local value="$1"
    if [[ "${value}" =~ ^[a-zA-Z0-9.\\-]+$ ]]; then
        return 0
    else
        echo "version is invalid" >&2
        exit 1
    fi
}

main "$@"
