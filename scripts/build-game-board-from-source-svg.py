#!/usr/bin/env python3

import logging
import json
import re
import shutil
import tempfile

from argparse import ArgumentParser
from contextlib import contextmanager
from dataclasses import dataclass
from xml.etree import ElementTree as ET

from selenium import webdriver


logger = logging.getLogger('board')
logger.addHandler(logging.StreamHandler())


SQUARE_SIZE_ELEMENT_ATTRIB = {
   'width': '46',
   'height': '46',
   'x': '396',
   'y': '508',
   'style': 'stroke-width:0.9883281',
}
LAST_LINE_SQUARE_TYPE = 'temple'
HIGHLIGHT_SQUARE_SYMBOL_ID = '#Highlight'

SVG_NAMESPACE = 'http://www.w3.org/2000/svg'
XLINK_NAMESPACE = 'http://www.w3.org/1999/xlink'
ET.register_namespace('', SVG_NAMESPACE)
ET.register_namespace('cc', 'http://creativecommons.org/ns#')
ET.register_namespace('dc', 'http://purl.org/dc/elements/1.1/')
ET.register_namespace('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
ET.register_namespace('xlink', XLINK_NAMESPACE)


@dataclass(frozen=True)
class BBox:
    x_min: float
    x_max: float
    y_min: float
    y_max: float


@dataclass(frozen=True)
class Rect:
    height: float = 0
    width: float = 0
    x: float = 0
    y: float = 0


def main(args):
    working_board_file_path = prepare_board_file(args.file[0], args.squares_layer_id, args.square_size_id)
    old_square_ids_to_new_square_ids = get_square_ids_for_game_board(
        working_board_file_path,
        args.squares_layer_id,
        args.square_size_id,
    )
    game_board_data = get_game_board_data(
        set(old_square_ids_to_new_square_ids.values()),
        args.departure_squares,
    )
    with open(args.board_data_output, 'w') as board_data_file:
        board_data_file.write(json.dumps(game_board_data, indent=4))
    complete_board_file(
        working_board_file_path,
        args.board_output,
        args.squares_layer_id,
        args.square_size_id,
        args.pawns_layer_id,
        args.background_layer_id,
        old_square_ids_to_new_square_ids,
    )


def prepare_board_file(source_file_path, squares_layer_id, square_size_id):
    _, working_file_path = tempfile.mkstemp(suffix='.svg')
    shutil.copy(source_file_path, working_file_path)
    tree = ET.parse(working_file_path)
    root = tree.getroot()
    # Set an id for all squares.
    id_ = 0
    for square_container in root.find(f'.//*[@id="{squares_layer_id}"]').findall('./*'):
        square = _get_square_from_square_container(square_container)
        symbol_name = square.get(f'{{{XLINK_NAMESPACE}}}href').replace('#', '').replace('-symbol', '')
        square_container.set('id', f'{id_}--{symbol_name}')
        id_ += 1
    # Append the square size element if not present.
    if root.find(f'.//*[@id="{square_size_id}"]') is None:
        ET.SubElement(
            root, 'rect',
            attrib={
                **SQUARE_SIZE_ELEMENT_ATTRIB,
                'id': square_size_id,
            },
        )

    # Save.
    data = ET.tostring(root, encoding='utf-8')
    with open(working_file_path, 'wb') as file:
        file.write(data)

    return working_file_path


def get_square_ids_for_game_board(file_path, squares_layer_id, square_size_id):
    square_old_ids_to_client_rect, square_size = parse_svg_in_browser(
        file_path,
        squares_layer_id,
        square_size_id,
    )
    board_bbox = get_board_bbox(square_old_ids_to_client_rect)
    return get_proper_square_ids(square_old_ids_to_client_rect, board_bbox, square_size)


@contextmanager
def open_web_browser():
    options = webdriver.FirefoxOptions()
    options.headless = True
    driver = webdriver.Firefox(options=options)
    try:
        yield driver
    except Exception:
        logger.exception('An error occurred while using the driver.')
    finally:
        driver.close()


def parse_svg_in_browser(svg_file_path, squares_layer_id, square_size_id):
    square_old_ids_to_client_rect = {}
    square_size = Rect()
    get_bounding_client_rect_js_template = 'return document.getElementById("{element_id}").getBoundingClientRect();'

    with open_web_browser() as browser:
        browser.get(f'file://{svg_file_path}')

        squares_layer = browser.find_element_by_id(squares_layer_id)

        logger.debug(f'Size squares layer: {browser.execute_script(get_bounding_client_rect_js_template.format(element_id="squares"))}')  # noqa: E501
        logger.debug(f'Size square size: {browser.execute_script(get_bounding_client_rect_js_template.format(element_id=square_size_id))}')  # noqa: E501

        for square in squares_layer.find_elements_by_xpath('./*'):
            square_id = square.get_attribute('id')
            if not square_id:
                # This should never happen, but if it does, fail in a way we can understand.
                raise AssertionError('A square does not have an id. Cannot continue.')
            client_rect_data = browser.execute_script(
                get_bounding_client_rect_js_template.format(element_id=square_id),
            )
            square_old_ids_to_client_rect[square_id] = Rect(
                x=client_rect_data['x'],
                y=client_rect_data['y'],
                width=client_rect_data['width'],
                height=client_rect_data['height'],
            )

        client_rect_data = browser.execute_script(
            get_bounding_client_rect_js_template.format(element_id=square_size_id),
        )
        square_size = Rect(
            x=client_rect_data['x'],
            y=client_rect_data['y'],
            width=client_rect_data['width'],
            height=client_rect_data['height'],
        )

    return square_old_ids_to_client_rect, square_size


def get_board_bbox(square_old_ids_to_client_rect):
    x_min = float('inf')
    y_min = float('inf')
    x_max = 0
    y_max = 0

    for square_rect in square_old_ids_to_client_rect.values():
        x_min = min(x_min, square_rect.x)
        x_max = max(x_max, square_rect.x)
        y_min = min(y_min, square_rect.y)
        y_max = max(y_max, square_rect.y)

    bbox = BBox(x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max)
    logger.debug(f'BBox: {bbox}')
    return bbox


def get_proper_square_ids(square_old_ids_to_client_rect, board_bbox, square_size):
    old_ids_to_new_ids = {}

    x = board_bbox.x_min
    # This is the id we will use to identify the square along the X axis.
    square_x_id = 1
    # We add some tolerance here to account for square misalignment and to be sure to get the last line. Same for y.
    while x < (board_bbox.x_max + square_size.width):
        # Starting a new column, reset all y values.
        y = board_bbox.y_min
        # This is the id we will use to identify the square along the Y axis.
        square_y_id = 1
        while y < (board_bbox.y_max + square_size.height):
            for square_old_id, square_rect in square_old_ids_to_client_rect.items():
                current_rect = Rect(x=x, y=y, width=square_size.width, height=square_size.height)
                if is_square_center_in_current_rect(square_rect, current_rect):
                    old_ids_to_new_ids[square_old_id] = get_square_id(square_x_id, square_y_id, square_old_id)
            y += square_size.height
            square_y_id += 1
        x += square_size.width
        square_x_id += 1

    if len(old_ids_to_new_ids) != len(square_old_ids_to_client_rect):
        for square_id in set(square_old_ids_to_client_rect.keys()) - set(old_ids_to_new_ids.keys()):
            logger.debug(f'{square_old_ids_to_client_rect[square_id]} is missing.')
        raise AssertionError(
            f'You could not attribute an proper id to all the squares! '
            f'Total squares: {len(square_old_ids_to_client_rect)}. With proper id: {len(old_ids_to_new_ids)}'
        )

    return old_ids_to_new_ids


def is_square_center_in_current_rect(square_rect, current_rect):
    # We allow some uncertainty on where the actual center of the square is.
    x_precision = current_rect.width // 2
    y_precision = current_rect.height // 2

    x_lower_bound = current_rect.x - x_precision
    x_upper_bound = current_rect.x + x_precision
    y_lower_bound = current_rect.y - y_precision
    y_upper_bound = current_rect.y + y_precision

    return (
        x_lower_bound < square_rect.x < x_upper_bound
        and y_lower_bound < square_rect.y < y_upper_bound
    )


def get_square_id(x, y, old_id) -> str:
    square_type = old_id.split('--')[1]
    return f'square--{square_type}--{x}-{y}'


def get_x_y_from_square_id(square_id: str) -> (int, int):
    match = re.search(r'(\d+)-(\d+)$', square_id)
    x = int(match.group(1))
    y = int(match.group(2))
    return x, y


def get_type_from_square_id(square_id: str) -> str:
    return square_id.split('--')[1]


def get_game_board_data(square_ids: set, departure_squares: list) -> dict:
    """Return the list of lists of square for the game.

    - Existing squares are represented by a dict.
    - Missing squares are represented by None.

    Example: ``{ 'board': [[None, {'id': 'square-2-2', 'type': 'forest'}, None]], 'square-types': ['forest'] }``
    """
    board = []
    xy_to_ids = {}
    square_types = set()

    max_x_id = 1
    max_y_id = 1
    for square_id in square_ids:
        x, y = get_x_y_from_square_id(square_id)
        max_x_id = max(max_x_id, x)
        max_y_id = max(max_y_id, y)

        xy_to_ids[(x, y)] = square_id

    for x in range(1, max_x_id + 1):
        for y in range(1, max_y_id + 1):
            if (x, y) in xy_to_ids:
                type_ = get_type_from_square_id(xy_to_ids[(x, y)])
                is_arrival = LAST_LINE_SQUARE_TYPE in type_
                type_ = type_.replace('-temple', '')  # Temple squares are a subtype used only for display.
                square_types.add(type_)
                board.append({
                    'id': xy_to_ids[(x, y)],
                    'x': x,
                    'y': y,
                    'type': type_,
                    'is-arrival': is_arrival,
                    'is-departure': f'{x},{y}' in departure_squares,
                })
            else:
                board.append({
                    'id': None,
                    'x': x,
                    'y': y,
                    'type': None,
                    'is-arrival': False,
                    'is-departure': False,
                })

    test_board = []
    for x in range(0, 32):
        for y in range(0, 32):
            if (x, y) in xy_to_ids:
                type_ = get_type_from_square_id(xy_to_ids[(x, y)])
            else:
                type_ = 'forest'
            is_arrival = LAST_LINE_SQUARE_TYPE in type_
            type_ = type_.replace('-temple', '')  # Temple squares are a subtype used only for display.
            square_types.add(type_)
            test_board.append({
                'id': xy_to_ids.get((x, y)),
                'x': x,
                'y': y,
                'type': type_,
                'is-arrival': is_arrival,
                'is-departure': f'{x},{y}' in departure_squares,
            })

    return {
        'board': {
            'squares': board,
            'squares-types-to-colors': {
                'mountain': 'BLACK',
                'water': 'BLUE',
                'desert': 'YELLOW',
                'forest': 'RED',
            },
        },
        'test-board': {
            'board': {
                'squares': test_board,
                'squares-types-to-colors': {
                    'mountain': 'BLACK',
                    'water': 'BLUE',
                    'desert': 'YELLOW',
                    'forest': 'RED',
                },
            },
        },
    }


def complete_board_file(
    working_file_path,
    output_file_path,
    squares_layer_id,
    square_size_id,
    pawns_layer_id,
    background_layer_id,
    old_square_ids_to_new_square_ids,
):
    tree = ET.parse(working_file_path)
    root = tree.getroot()
    # Set an id for all squares.
    for square_container in root.find(f'.//*[@id="{squares_layer_id}"]').findall('./*'):
        new_id = old_square_ids_to_new_square_ids[square_container.get('id')]
        type_ = get_type_from_square_id(new_id)
        is_arrival_square = LAST_LINE_SQUARE_TYPE in type_
        new_id = new_id.replace(f'--{type_}--', '-')
        x, y = get_x_y_from_square_id(new_id)
        square_container.set('id', new_id)
        square_container.set('data-type', type_)
        square = _get_square_from_square_container(square_container)
        square.set('class', 'square')
        highlight = _get_highlight_from_square_container(square_container)
        highlight.set('class', 'square-highlight')
        if is_arrival_square:
            # We cannot change the color of arrival squares.
            square_container.set('click.delegate', f"handleSquareClicked('{new_id}', '{x}', '{y}', {{isArrivalSquare: true}})")
            square_container.set('class', f"square-container arrival-square ${{possibleSquares.includes('{new_id}') ? 'highlighted' : ''}}")
        else:
            default_color = square.get(f'{{{XLINK_NAMESPACE}}}href')
            square.set(f'{{{XLINK_NAMESPACE}}}href', f"${{squaresToColors['{new_id}'] || '{default_color}'}}")
            square_container.set('click.delegate', f"handleSquareClicked('{new_id}', '{x}', '{y}', {{isArrivalSquare: false}})")
            square_container.set('class', f"square-container ${{possibleSquares.includes('{new_id}') ? 'highlighted' : ''}}")

    for num, pawn in enumerate(root.find(f'.//*[@id="{pawns_layer_id}"]').findall('./*')):
        pawn.set('if.bind', f"playerIndexes.includes({num})")  # noqa: E501
        pawn.set('class', f"${{clickablePawnIndexes.includes({num}) ? 'pointer' : ''}}")
        pawn.set('click.delegate', f"pawnClicked({num})")
        pawn.set('id', f"player{num}Container")
        pawn.set('mouseleave.trigger', 'hidePlayerName()')
        pawn.set('mouseenter.trigger', f"showPlayerName({num}, $event)")
        pawn.set(f'{{{XLINK_NAMESPACE}}}href.bind', f"assetSource.forPawn(heroes[{num}])")
        pawn.attrib.pop(f'{{{XLINK_NAMESPACE}}}href')
        pawn.attrib.pop('transform')

    for image in root.find(f'.//*[@id="{background_layer_id}"]').findall('./*'):
        current_image = image.get(f'{{{XLINK_NAMESPACE}}}href').split('.')[0]
        image.set(
            f'{{{XLINK_NAMESPACE}}}href',
            f"../../../../assets/game/backgrounds/{current_image}.jpeg",
        )
        image.attrib.pop(f'{{{XLINK_NAMESPACE}}}href')

    squares_layer = root.find(f'.//*[@id="{squares_layer_id}"]')
    squares_layer.set('ref', 'squaresLayer')
    pawns_layer = root.find(f'.//*[@id="{pawns_layer_id}"]')
    pawns_layer.set('ref', 'pawnLayer')

    root.set('id', 'board')
    square_size = root.find(f'./*[@id="{square_size_id}"]')
    root.remove(square_size)
    title = root.find(f'{{{SVG_NAMESPACE}}}title')
    if title:
        root.remove(title)

    # Remove all clipPath to reduce file size and improve performance in browser.
    defs_layer = root.find(f'./{{{SVG_NAMESPACE}}}defs')
    if defs_layer:
        for clipPath in defs_layer.findall(f'./{{{SVG_NAMESPACE}}}clipPath'):
            defs_layer.remove(clipPath)

    # Save.
    data = ET.tostring(root, encoding='utf-8')
    with open(output_file_path, 'wb') as file:
        file.write(data)


def _get_square_from_square_container(square_container):
    for child in square_container:
        if child.get(f'{{{XLINK_NAMESPACE}}}href') != HIGHLIGHT_SQUARE_SYMBOL_ID:
            return child


def _get_highlight_from_square_container(square_container):
    for child in square_container:
        if child.get(f'{{{XLINK_NAMESPACE}}}href') == HIGHLIGHT_SQUARE_SYMBOL_ID:
            return child


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        'file',
        nargs=1,
        help='Path to the SVG file',
    )
    parser.add_argument(
        '--squares-layer-id',
        help='Id of the squares layer',
        default='squares',
        dest='squares_layer_id',
    )
    parser.add_argument(
        '--square-size-id',
        help='Id of the element that determines the size of a square',
        default='square-size',
        dest='square_size_id',
    )
    parser.add_argument(
        '--pawns-layer-id',
        help='Id of the pawns layer',
        default='pawns',
        dest='pawns_layer_id',
    )
    parser.add_argument(
        '--background-layer-id',
        help='Id of the background layer',
        default='background',
        dest='background_layer_id',
    )
    parser.add_argument(
        '--log-level',
        help='Log level to use, from 10 (debug) to 50 (critical)',
        type=int,
        default=20,
        dest='log_level',
    )
    parser.add_argument(
        '--board-data-output',
        help='Where to store the data about the board',
        default='game/board-data.json',
        dest='board_data_output',
    )
    parser.add_argument(
        '--board-output',
        help='Where to store the generated board',
        default='game/board.svg',
        dest='board_output',
    )
    parser.add_argument(
        '--departure-squares',
        help='Squares that should be used for departure, separate by a space. For instance: x1,y1 x2,y2',
        dest='departure_squares',
        nargs='*',
    )

    args = parser.parse_args()
    logger.setLevel(args.log_level)

    main(args)
