---
- hosts: localhost
  gather_facts: no
  vars:
    # If the version of the API is not defined, we must connect to the latest version.
    # We cannot simply use the version because the front can have a version that is different
    # of the version of the API when we deploy only the frontend.
    api_version: latest
  tasks:
    - name: Check version is defined
      tags:
        - front
        - check
      fail: msg="Bailing out. this play requires the variable 'version'"
      when: version is not defined
    - name: Ansible check directory exists
      tags:
        - front
        - check
      stat:
        path: ./front/
      register: front_directory
    - name: Check that the frontend directory is here
      tags:
        - front
        - check
      fail: msg="Front directory is missing. Cannot continue."
      when: not front_directory.stat.exists
    - name: Version info
      debug: msg="Building frontend with version={{ version }} and api_version={{ api_version }}"
    - name: Clean previous builds of the frontend
      tags:
        - front
        - clean
      command: "nps clean"
      args:
        chdir: "{{ src.front }}"
        removes: "dist"
    - name: Build the frontend
      tags:
        - front
        - build
      command: "nps 'build.config --env {{ env }} --version {{ version }} --api-version {{ api_version }}' 'webpack.build --env.environment={{ env }}' 'build.manifest --env {{ env }} --version {{ version }}'"
      register: frontend_build
      args:
        chdir: "{{ src.front }}"
        creates: "dist"
      environment:
        NODE_OPTIONS: "--openssl-legacy-provider"
    - name: Print build output
      tags:
        - front
        - build
      debug:
        msg: "{{ frontend_build.stdout }}"
        verbosity: 1
    - name: Add git tag
      when: env == 'prod'
      shell:
        cmd: "git tag {{ version }} --force && git push --tags --no-verify"
        chdir: "{{ src.front }}"
      tags:
        - skip_ansible_lint
- hosts: fronts
  tasks:
    - name: Create destination deploy directory
      tags:
        - front
        - deploy
      file:
        path: "{{ dest.front }}"
        state: directory
        recurse: true
    - name: Copy built files
      tags:
        - front
        - deploy
      synchronize:
        src: "{{ src.front }}/dist/"
        dest: "{{ dest.front }}/"
        delete: false
    - name: Allow direct access of last deployed version
      tags:
        - front
        - deploy
      copy:
        src: "{{ src.front }}/dist/index.html"
        dest: "{{ dest.front }}/index-{{ version }}.html"
    - name: Copy robot.txt
      tags:
        - front
        - deploy
      template:
        src: templates/robots/front.txt.j2
        dest: "{{ dest.front }}/robots.txt"
    - name: Make sure permisions are correct
      tags:
        - front
        - deploy
      file:
        path: "{{ dest.front }}"
        state: directory
        recurse: true
        mode: "u=rwX,g=rwX,o=rX"
